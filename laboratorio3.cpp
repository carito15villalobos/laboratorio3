#include <iostream>
#include "Cake.h"
#include "ChocolateCake.h"
#include "VanillaCake.h"
#include "Factory.h"
using namespace std;

Cake* Factory::obj = nullptr;

int main()
{

    Cake* Cobj = Factory::getIns("vanilla");
    Cake* Cobj1 = Factory::getIns("choc");

    Cobj->recipe();
    Cobj1->recipe();

    return 0;
}