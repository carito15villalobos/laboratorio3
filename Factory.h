#pragma once
#include "Cake.h"
#include "ChocolateCake.h"
#include "VanillaCake.h"
#include <iostream>
#include <string>
using namespace std;
class Factory
{
    private:
        static Cake* obj;
    public:

        void factory()
        {
            std::cout << "Const : \n";
        }

        static Cake* getIns(string type)
        {
            if (type == "choc")
            {
                obj = new ChocolateCake();
            }
            else if (type == "vanilla")
            {
                obj = new VanillaCake();
            }

            return obj;
        }
};

